package com.mbp.monitoringsale.config;

import com.mbp.monitoringsale.enumeration.TranslateEnum;
import com.mbp.monitoringsale.model.DataResult;
import com.mbp.monitoringsale.model.Model;
import com.mbp.monitoringsale.service.MonitoringSale;
import com.mbp.monitoringsale.translator.GenericTranslator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.dsl.Pollers;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.FileWritingMessageHandler;
import org.springframework.integration.file.dsl.Files;
import org.springframework.integration.file.support.FileExistsMode;
import org.springframework.integration.handler.GenericHandler;
import org.springframework.integration.transformer.GenericTransformer;

import java.nio.file.Paths;
import java.util.List;
import java.util.Map;

@Slf4j
@Configuration
@EnableIntegration
public class IntegrationConfig {

    @Value("${file.dir.in}")
    private String IN_DIR;

    @Value("${file.dir.out}")
    private String OUT_DIR;

    @Value("${file.dir.separator}")
    private String LINE_SEPARATOR;

    @Value("${integration.config.poller}")
    private int POLLER;

    private final MonitoringSale monitoringSale;
    private Map<String, GenericTranslator<String[], ? extends Model>> translatorMap;

    @Autowired
    public IntegrationConfig(Map<String, GenericTranslator<String[], ? extends Model>> translatorMap,
                             MonitoringSale monitoringSale) {
        this.monitoringSale = monitoringSale;
        this.translatorMap = translatorMap;
    }

    @Bean
    public IntegrationFlow flow() {
        return IntegrationFlows
                .from(fileReadingMessageSource(), confg -> confg.poller(Pollers.fixedRate(POLLER).get()))
                .transform(Files.toStringTransformer("UTF-8"))
                .split(splitter -> splitter.delimiters(System.lineSeparator()))
                .transform(translating())
                .aggregate()
                .handle(processing())
                .handle(generatResult())
                .handle(fileWritingMessageHandler())
                .get();
    }

    /**
     * Process that performs the reading of the line, divides it by the separator character.
     * Forwards to "Translates" by transforming the String to a Model Object.
     *
     * @return
     */
    private GenericTransformer<String, ? extends Model> translating() {
        return (String source) -> {
            String[] line = source.split(LINE_SEPARATOR);

            if (line[0].length() != 3) {
                log.error("Incorrect code in line. -> ", line[0]);
                return null;
            }

            String componentName = TranslateEnum.translatorWithCode(line[0]).getTranslateName();
            return translatorMap.get(componentName).process(line);
        };
    }

    /**
     * Calls the Processing method passing to process the collected data.
     *
     * @return
     */
    @Bean
    public GenericHandler<List<Model>> processing() {
        return (message, headers) -> {
            return monitoringSale.processing(message);
        };
    }

    /**
     * Calls the GenerateResult method to generate the processing result.
     *
     * @return
     */
    @Bean
    public GenericHandler<DataResult> generatResult() {
        return (message, headers) -> {
            return monitoringSale.generateResult(message);
        };
    }

    /**
     * Load the files to be read by the specified path.
     *
     * @return
     */
    @Bean
    public FileReadingMessageSource fileReadingMessageSource() {
        return Files.inboundAdapter(Paths.get(this.IN_DIR).toFile())
                .useWatchService(true)
                .get();
    }

    /**
     * Generates the new file with the result of the processing.
     * If you have a file with the same name and replaced.
     *
     * @return
     */
    @Bean
    public FileWritingMessageHandler fileWritingMessageHandler() {
        return Files.outboundAdapter(Paths.get(this.OUT_DIR).toFile())
                .deleteSourceFiles(true)
                .fileExistsMode(FileExistsMode.REPLACE)
                .get();
    }

}
