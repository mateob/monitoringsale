package com.mbp.monitoringsale.service;

import com.mbp.monitoringsale.model.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class MonitoringSale {

    private static final String SEPARATOR = System.lineSeparator();

    private int customersCount = 0;
    private int salesmansCount = 0;
    private int mostExprensiveSaleID = 0;
    private String worstSalesmanName = null;
    private BigDecimal maxSaleAmount = BigDecimal.ZERO;
    private BigDecimal minSaleAmount = BigDecimal.ZERO;

    /**
     * Processes the data collected from the file thus generating the result of the data analysis.
     *
     * @param models
     * @return
     */
    public DataResult processing(List<Model> models) {
        if (CollectionUtils.isEmpty(models)) {
            return DataResult.builder().build();
        }

        for (Model model : models) {
            processingModels(model);
        }

        return DataResult.builder()
                .amountOfCustomers(this.customersCount)
                .amountOfSalesmans(this.salesmansCount)
                .mostExpensiveSaleId(this.mostExprensiveSaleID)
                .worstSalesmanName(this.worstSalesmanName)
                .build();
    }

    /**
     * Apply the following rules to the informed Model
     * if it is a Customer it adds +1 to the counter
     * if you are a Salesman add +1 to the counter
     * if it's a Sale, it captures the highest value and the seller who sold the least
     *
     * @param model
     */
    private void processingModels(Model model) {
        if (model instanceof Customer) {
            this.customersCount++;
        } else if (model instanceof Salesman) {
            this.salesmansCount++;
        } else if (model instanceof Sale) {
            Sale sale = (Sale) model;
            BigDecimal totalAmount = sale.getTotalAmount();
            if (totalAmount.compareTo(maxSaleAmount) == 1) {
                maxSaleAmount = totalAmount;
                this.mostExprensiveSaleID = sale.getId();
            }

            if (totalAmount.compareTo(minSaleAmount) == -1 || BigDecimal.ZERO.equals(minSaleAmount)) {
                minSaleAmount = totalAmount;
                this.worstSalesmanName = sale.getSalesmanName();
            }
        }
    }

    /**
     * Collect the data reported by DataResult.
     * Informs line by line each of the values and their respective message.
     *
     * @param dataResult
     * @return
     */
    public String generateResult(DataResult dataResult) {
        StringBuilder strBuilder = new StringBuilder();
        if (dataResult.getAmountOfCustomers() > 0) {
            strBuilder.append(
                    String.format("Quantidade de clientes no arquivo de entrada %s%s",
                            dataResult.getAmountOfCustomers(), SEPARATOR));
        }

        if (dataResult.getAmountOfSalesmans() > 0) {
            strBuilder.append(
                    String.format("Quantidade de vendedores no arquivo de entrada %s%s",
                            dataResult.getAmountOfSalesmans(), SEPARATOR));
        }

        if (dataResult.getMostExpensiveSaleId() > 0) {
            strBuilder.append(
                    String.format("ID da venda mais cara %S%S",
                            dataResult.getMostExpensiveSaleId(), SEPARATOR));
        }

        if (!StringUtils.isEmpty(dataResult.getWorstSalesmanName())) {
            strBuilder.append(
                    String.format("O pior vendedor %S%S",
                            dataResult.getWorstSalesmanName(), SEPARATOR));
        }

        return strBuilder.toString();
    }
}
