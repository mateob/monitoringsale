package com.mbp.monitoringsale.translator;

public interface GenericTranslator<S, T> {
    T process(S line);
}
