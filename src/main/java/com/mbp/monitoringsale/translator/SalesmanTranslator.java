package com.mbp.monitoringsale.translator;

import com.mbp.monitoringsale.model.Salesman;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component(value = "salesmanTranslator")
public class SalesmanTranslator implements GenericTranslator<String[], Salesman> {

    @Override
    public Salesman process(String[] line) {
        if (line == null) {
            return null;
        }

        return Salesman.builder()
                .cpf(line[1])
                .name(line[2])
                .salary(BigDecimal.valueOf(Double.parseDouble(line[3])).setScale(2))
                .build();
    }
}
