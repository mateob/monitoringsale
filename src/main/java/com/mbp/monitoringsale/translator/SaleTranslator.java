package com.mbp.monitoringsale.translator;

import com.mbp.monitoringsale.model.Sale;
import com.mbp.monitoringsale.model.SaleItem;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Component(value = "saleTranslator")
public class SaleTranslator implements GenericTranslator<String[], Sale> {

    private static final String ITEM_SEPARATOR = "-";
    private static final String REGEX = "\\[(.*)\\]";

    @Override
    public Sale process(String[] line) {
        if (line == null) {
            return null;
        }

        return Sale.builder()
                .id(Integer.valueOf(line[1]))
                .itens(this.translatItems(line[2].replaceAll(REGEX, "$1")))
                .salesmanName(line[3])
                .build();
    }

    private List<SaleItem> translatItems(String items) {
        if (items.isEmpty()) {
            return null;
        }

        List<SaleItem> saleItemList = new ArrayList<>();

        String[] itemList = items.split(",");

        for (String item : itemList) {
            String[] saleItem = item.split(ITEM_SEPARATOR);
            saleItemList.add(SaleItem.builder()
                    .id(Integer.parseInt(saleItem[0]))
                    .quantity(Long.parseLong(saleItem[1]))
                    .price(BigDecimal.valueOf(Double.parseDouble(saleItem[2])))
                    .build());
        }
        return saleItemList;
    }
}
