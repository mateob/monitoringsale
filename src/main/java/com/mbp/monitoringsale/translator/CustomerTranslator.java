package com.mbp.monitoringsale.translator;

import com.mbp.monitoringsale.model.Customer;
import org.springframework.stereotype.Component;

@Component(value = "customerTranslator")
public class CustomerTranslator implements GenericTranslator<String[], Customer> {

    @Override
    public Customer process(String[] line) {
        if (line == null) {
            return null;
        }

        return Customer.builder()
                .cnpj(line[1])
                .name(line[2])
                .bussinesArea(line[3])
                .build();
    }
}
