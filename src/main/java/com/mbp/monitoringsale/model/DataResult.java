package com.mbp.monitoringsale.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DataResult extends Model {

    private int amountOfCustomers;
    private int amountOfSalesmans;
    private int mostExpensiveSaleId;
    private String worstSalesmanName;
}
