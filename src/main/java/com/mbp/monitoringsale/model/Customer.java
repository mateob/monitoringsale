package com.mbp.monitoringsale.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Customer extends Model {

    private String cnpj;
    private String name;
    private String bussinesArea;

}


