package com.mbp.monitoringsale.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Builder
public class Sale extends Model {

    private int id;
    private List<SaleItem> itens;
    private String salesmanName;

    /**
     * Returns the total value of sales.
     * Performs the sum of each item sold multiplying by the quantity.
     *
     * @return
     */
    public BigDecimal getTotalAmount() {
        BigDecimal totalAmount = BigDecimal.ZERO;
        if (CollectionUtils.isEmpty(itens)) {
            return totalAmount;
        }

        for (SaleItem item : this.getItens()) {
            if (item == null) {
                continue;
            }
            if (item.getPrice() == BigDecimal.ZERO || item.getQuantity() == 0) {
                continue;
            }

            totalAmount = totalAmount.add(item.getPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
        }
        return totalAmount;
    }
}
