package com.mbp.monitoringsale.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class Salesman extends Model {

    private String cpf;
    private String name;
    private BigDecimal salary;

}
