package com.mbp.monitoringsale.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
public class SaleItem extends Model {

    private int id;
    private long quantity;
    private BigDecimal price;
}
