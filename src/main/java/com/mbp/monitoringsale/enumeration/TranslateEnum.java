package com.mbp.monitoringsale.enumeration;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TranslateEnum {
    SALESMAN("001", "salesmanTranslator"),
    CUSTOMER("002", "customerTranslator"),
    SALE("003", "saleTranslator");

    private String code;
    private String translateName;

    public static TranslateEnum translatorWithCode(String code) {
        if (code == null || code.isEmpty()) {
            return null;
        }
        for (TranslateEnum translate : TranslateEnum.values()) {
            if (translate.code.equals(code)) {
                return translate;
            }
        }
        return null;
    }
}
