package com.mbp.monitoringsale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MonitoringSaleApplication {

	public static void main(String[] args) {
		SpringApplication.run(MonitoringSaleApplication.class, args);
	}

}
