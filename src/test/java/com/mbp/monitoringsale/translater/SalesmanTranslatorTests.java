package com.mbp.monitoringsale.translater;

import com.mbp.monitoringsale.model.Salesman;
import com.mbp.monitoringsale.translator.SalesmanTranslator;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;

@ExtendWith(MockitoExtension.class)
public class SalesmanTranslatorTests {

    @InjectMocks
    private SalesmanTranslator salesmanTranslator;

    @Test
    void translateOk() {
        String code = "001";
        String cpf = "014725836912";
        String name = "Roberto Castro";
        String salary = "5213.22";
        String[] source = new String[]{code, cpf, name, salary};
        Salesman salesman = salesmanTranslator.process(source);

        Assert.assertNotNull(salesman);
        Assert.assertEquals(cpf, salesman.getCpf());
        Assert.assertEquals(name, salesman.getName());
        Assert.assertEquals(BigDecimal.valueOf(Double.parseDouble(salary)), salesman.getSalary());
    }

    @Test
    void translateNull() {
        Salesman salesman = salesmanTranslator.process(null);

        Assert.assertNull(salesman);
    }
}
