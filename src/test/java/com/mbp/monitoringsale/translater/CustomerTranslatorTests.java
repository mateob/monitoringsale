package com.mbp.monitoringsale.translater;

import com.mbp.monitoringsale.model.Customer;
import com.mbp.monitoringsale.translator.CustomerTranslator;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CustomerTranslatorTests {

    @InjectMocks
    private CustomerTranslator customerTranslator;

    @Test
    void translateOk() {
        String code = "002";
        String cnpj = "1234567891425369";
        String name = "Pedro Henrique";
        String area = "Rural";
        String[] customerLine = new String[]{code, cnpj, name, area};
        Customer customer = customerTranslator.process(customerLine);

        Assert.assertNotNull(customer);
        Assert.assertEquals(cnpj, customer.getCnpj());
        Assert.assertEquals(name, customer.getName());
        Assert.assertEquals(area, customer.getBussinesArea());
    }

    @Test
    void translateNull() {
        Customer customer = customerTranslator.process(null);

        Assert.assertNull(customer);
    }
}
