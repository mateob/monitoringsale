package com.mbp.monitoringsale.translater;

import com.mbp.monitoringsale.model.Sale;
import com.mbp.monitoringsale.translator.SaleTranslator;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SaleTranslatorTests {

    @InjectMocks
    private SaleTranslator saleTranslator;

    @Test
    void translateOk() {
        String code = "003";
        String id = "10";
        String items = "[1-34-10,2-33-1.50,3-40-0.10]";
        String salesmanName = "Roberto Castro";
        String[] source = new String[]{code, id, items, salesmanName};
        Sale sale = saleTranslator.process(source);

        Assert.assertNotNull(sale);
        Assert.assertEquals(Integer.parseInt(id), sale.getId());
        Assert.assertEquals(3, sale.getItens().size());
        Assert.assertEquals(salesmanName, sale.getSalesmanName());
    }

    @Test
    void translateNull() {
        Sale sale = saleTranslator.process(null);

        Assert.assertNull(sale);
    }
}
