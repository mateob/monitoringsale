package com.mbp.monitoringsale;

import com.mbp.monitoringsale.model.*;
import com.mbp.monitoringsale.service.MonitoringSale;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class MonitoringSaleApplicationTests {

	@InjectMocks
	private MonitoringSale monitoringSale;

	/**
	 * Test's of Empty Model's
	 */

	@Test
	void transformCustomerEmpty() {
		Customer customer = Customer.builder().build();
		List<Model> modelList = Collections.singletonList(customer);

		DataResult dataResult = monitoringSale.processing(modelList);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(1, dataResult.getAmountOfCustomers());
		Assert.assertEquals(0, dataResult.getAmountOfSalesmans());
		Assert.assertEquals(0, dataResult.getMostExpensiveSaleId());
		Assert.assertNull(dataResult.getWorstSalesmanName());
	}

	@Test
	void transformSalesmanEmpty() {
		Salesman salesman = Salesman.builder().build();
		List<Model> modelList = Collections.singletonList(salesman);

		DataResult dataResult = monitoringSale.processing(modelList);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(0, dataResult.getAmountOfCustomers());
		Assert.assertEquals(1, dataResult.getAmountOfSalesmans());
		Assert.assertEquals(0, dataResult.getMostExpensiveSaleId());
		Assert.assertNull(dataResult.getWorstSalesmanName());
	}

	@Test
	void transformSaleEmpty() {
		SaleItem saleItem = SaleItem.builder().build();
		List<SaleItem> listItems = Collections.singletonList(saleItem);

		Sale sale = Sale.builder().itens(listItems).build();

		List<Model> listModels = Collections.singletonList(sale);
		DataResult dataResult = monitoringSale.processing(listModels);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(0, dataResult.getAmountOfCustomers());
		Assert.assertEquals(0, dataResult.getAmountOfSalesmans());
		Assert.assertEquals(0, dataResult.getMostExpensiveSaleId());
		Assert.assertNull(dataResult.getWorstSalesmanName());
	}

	@Test
	void processingModelEmpty() {
		List modelList = Collections.EMPTY_LIST;
		DataResult dataResult = monitoringSale.processing(modelList);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(0, dataResult.getAmountOfCustomers());
		Assert.assertEquals(0, dataResult.getAmountOfSalesmans());
		Assert.assertEquals(0, dataResult.getMostExpensiveSaleId());
		Assert.assertNull(dataResult.getWorstSalesmanName());
	}

	@Test
	void processingModelNull() {
		DataResult dataResult = monitoringSale.processing(null);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(0, dataResult.getAmountOfCustomers());
		Assert.assertEquals(0, dataResult.getAmountOfSalesmans());
		Assert.assertEquals(0, dataResult.getMostExpensiveSaleId());
		Assert.assertNull(dataResult.getWorstSalesmanName());
	}

	/**
	 * Correctli results.
	 */
	@Test
	void processingSaleAndSaleItems() {
		// Sale 01
		List<SaleItem> saleItemList1 = new ArrayList<>();
		saleItemList1.add(SaleItem.builder()
				.id(10)
				.quantity(20)
				.price(new BigDecimal(200))
				.build());
		saleItemList1.add(SaleItem.builder()
				.id(8)
				.quantity(50)
				.price(new BigDecimal(250.92))
				.build());
		Sale sale1 = Sale.builder()
				.id(100)
				.itens(saleItemList1)
				.salesmanName("Pedro")
				.build();
		// Sale 02
		List<SaleItem> saleItemList2 = new ArrayList<>();
		saleItemList2.add(SaleItem.builder()
				.id(20)
				.quantity(10)
				.price(new BigDecimal(125.30))
				.build());
		saleItemList2.add(SaleItem.builder()
				.id(18)
				.quantity(60)
				.price(new BigDecimal(530.22))
				.build());
		Sale sale2 = Sale.builder()
				.id(200)
				.itens(saleItemList2)
				.salesmanName("Raul")
				.build();

		List<Model> modelList = Arrays.asList(sale1, sale2);
		DataResult dataResult = monitoringSale.processing(modelList);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(0, dataResult.getAmountOfCustomers());
		Assert.assertEquals(0, dataResult.getAmountOfSalesmans());

		Assert.assertEquals(200, dataResult.getMostExpensiveSaleId());
		Assert.assertEquals("Pedro", dataResult.getWorstSalesmanName());
	}

	@Test
	void processingCustomerOk() {
		Customer customer = Customer.builder()
				.cnpj("1425364758693214")
				.name("Customer Test")
				.bussinesArea("Rural")
				.build();
		List<Model> modelList = Collections.singletonList(customer);
		DataResult dataResult = monitoringSale.processing(modelList);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(1, dataResult.getAmountOfCustomers());
		Assert.assertEquals(0, dataResult.getAmountOfSalesmans());
		Assert.assertEquals(0, dataResult.getMostExpensiveSaleId());
		Assert.assertNull(dataResult.getWorstSalesmanName());
	}

	@Test
	void processingSalesmanOk() {
		Salesman salesman = Salesman.builder()
				.cpf("3216457891526")
				.name("João")
				.salary(new BigDecimal(5234.12))
				.build();
		List<Model> modelList = Collections.singletonList(salesman);
		DataResult dataResult = monitoringSale.processing(modelList);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(0, dataResult.getAmountOfCustomers());
		Assert.assertEquals(1, dataResult.getAmountOfSalesmans());
		Assert.assertEquals(0, dataResult.getMostExpensiveSaleId());
		Assert.assertNull(dataResult.getWorstSalesmanName());
	}

	@Test
	void processingFullModelsOk() {
		// Sale 01
		List<SaleItem> saleItemList1 = new ArrayList<>();
		saleItemList1.add(SaleItem.builder().id(10).quantity(20).price(new BigDecimal(200)).build());
		saleItemList1.add(SaleItem.builder().id(8).quantity(50).price(new BigDecimal(250.92)).build());
		Sale sale1 = Sale.builder().id(100).itens(saleItemList1).salesmanName("Pedro").build();
		// Sale 02
		List<SaleItem> saleItemList2 = new ArrayList<>();
		saleItemList2.add(SaleItem.builder().id(20).quantity(10).price(new BigDecimal(125.30)).build());
		saleItemList2.add(SaleItem.builder().id(18).quantity(60).price(new BigDecimal(530.22)).build());
		Sale sale2 = Sale.builder().id(200).itens(saleItemList2).salesmanName("Raul").build();

		Customer customer = Customer.builder().cnpj("1425364758693214").name("Customer Test").bussinesArea("Rural").build();
		Salesman salesman = Salesman.builder().cpf("3216457891526").name("João").salary(new BigDecimal(5234.12)).build();
		List<Model> modelList = Arrays.asList(salesman, customer, sale1, sale2);

		DataResult dataResult = monitoringSale.processing(modelList);

		Assert.assertNotNull(dataResult);
		Assert.assertEquals(1, dataResult.getAmountOfCustomers());
		Assert.assertEquals(1, dataResult.getAmountOfSalesmans());
		Assert.assertEquals(200, dataResult.getMostExpensiveSaleId());
		Assert.assertEquals("Pedro", dataResult.getWorstSalesmanName());
	}

	/**
	 * Validate result of Models
	 */
	@Test
	void generateResultCountCustomers() {
		DataResult dataResult = DataResult.builder().amountOfCustomers(10).build();
		String result = monitoringSale.generateResult(dataResult);
		String[] source = result.split(System.lineSeparator());
		Assert.assertEquals(1, source.length);
		Assert.assertEquals("Quantidade de clientes no arquivo de entrada 10", source[0]);
	}

	@Test
	void generateResultCountSalesman() {
		DataResult dataResult = DataResult.builder().amountOfSalesmans(5).build();
		String result = monitoringSale.generateResult(dataResult);
		String[] source = result.split(System.lineSeparator());
		Assert.assertEquals(1, source.length);
		Assert.assertEquals("Quantidade de vendedores no arquivo de entrada 5", source[0]);
	}

	@Test
	void generateResultMostExpensiveSaleID() {
		DataResult dataResult = DataResult.builder().mostExpensiveSaleId(30).build();
		String result = monitoringSale.generateResult(dataResult);
		String[] source = result.split(System.lineSeparator());
		Assert.assertEquals(1, source.length);
		Assert.assertEquals("ID da venda mais cara 30", source[0]);
	}

	@Test
	void generateResultWorstSalesmanName() {
		DataResult dataResult = DataResult.builder().worstSalesmanName("PAULO").build();
		String result = monitoringSale.generateResult(dataResult);
		String[] source = result.split(System.lineSeparator());
		Assert.assertEquals(1, source.length);
		Assert.assertEquals("O pior vendedor PAULO", source[0]);
	}

	@Test
	void generateResultFull() {
		DataResult dataResult = DataResult.builder()
				.amountOfCustomers(5)
				.amountOfSalesmans(2)
				.mostExpensiveSaleId(20)
				.worstSalesmanName("PAULO")
				.build();

		String result = monitoringSale.generateResult(dataResult);
		String[] source = result.split(System.lineSeparator());
		Assert.assertEquals(4, source.length);
		Assert.assertEquals("Quantidade de clientes no arquivo de entrada 5", source[0]);
		Assert.assertEquals("Quantidade de vendedores no arquivo de entrada 2", source[1]);
		Assert.assertEquals("ID da venda mais cara 20", source[2]);
		Assert.assertEquals("O pior vendedor PAULO", source[3]);
	}
}
