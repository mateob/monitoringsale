# Objetivo da prova
O objetivo da prova é testarmos suas habilidades em desenvolvimento de software. Iremos
avaliar mais do que o funcionamento da solução proposta, avaliaremos a sua abordagem, a
sua capacidade analítica, boas práticas de engenharia de software, performance e
escalabilidade da solução.

# Tecnologias utilizadas
* Java 8
* Spring Boot
* Spring Integration
* JUnit
* Mockito

# Solução
Utilizei o Spring Integration para ter um melhor controle sobre o Flow do processo de leitura, transformação e escrita do resultado. 
O Framework efetua o controle deste Flow, desta forma não e necesario implementar todo o processo, ou a manupulação dos dados em si.

Foi utilizado Components para interagir com os modelos "Translator" para traduzir a String em Objeto conforme codigo de cada entidade. 